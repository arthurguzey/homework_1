package test.java.com.homework_1;

import homework_1.java.com.homework.Cycles;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class CyclesTest {
    Cycles cut = new Cycles();

    @Test
    void  getSumOfEvenFromOneToNinetynineTest(){
        int expected = 2450;
        double actual = cut.getSumOfEvenFromOneToNinetynine();
        Assertions.assertEquals(expected, actual);
    }


    static Arguments[] checkPrimeNumberTestArgs(){
        return new Arguments[]{
                Arguments.arguments(7, true),
                Arguments.arguments(12, false),
                Arguments.arguments(-7, true),
                Arguments.arguments(-12, false)
        };
    }
    @ParameterizedTest
    @MethodSource("checkPrimeNumberTestArgs")
    void checkPrimeNumberTest (int a, boolean expected)
    {
        boolean actual = cut.checkPrimeNumber(a);
        Assertions.assertEquals(expected, actual);
    }


    static Arguments[] getSqrtTestArgs(){
        return new Arguments[]{
                Arguments.arguments(-9, 0),
                Arguments.arguments(9, 3),
                Arguments.arguments(65, 8),
                Arguments.arguments(63, 7),
        };
    }
    @ParameterizedTest
    @MethodSource("getSqrtTestArgs")
    void getSqrtTest (int a, int expected)
    {
        int actual = cut.getSqrt(a);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getFactorialTestArgs(){
        return new Arguments[]{
                Arguments.arguments(-9, 0),
                Arguments.arguments(5, 120),
                Arguments.arguments(9, 362880),
        };
    }
    @ParameterizedTest
    @MethodSource("getFactorialTestArgs")
    void getFactorialTest (int a, int expected)
    {
        int actual = cut.getFactorial(a);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getSumOfDigitsTestArgs(){
        return new Arguments[]{
                Arguments.arguments(-19, 10),
                Arguments.arguments(12345, 15),
        };
    }
    @ParameterizedTest
    @MethodSource("getSumOfDigitsTestArgs")
    void getSumOfDigitsTest (int a, int expected)
    {
        int actual = cut.getSumOfDigits(a);
        Assertions.assertEquals(expected, actual);
    }


    static Arguments[] getMirrorNumberTestArgs(){
        return new Arguments[]{
                Arguments.arguments(-1945, 5491),
                Arguments.arguments(12345, 54321),
        };
    }
    @ParameterizedTest
    @MethodSource("getMirrorNumberTestArgs")
    void getMirrorNumberTest (int a, int expected)
    {
        int actual = cut.getMirrorNumber(a);
        Assertions.assertEquals(expected, actual);
    }


}


