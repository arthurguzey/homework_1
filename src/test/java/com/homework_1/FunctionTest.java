package test.java.com.homework_1;

import homework_1.java.com.homework.Function;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class FunctionTest {
    Function function = new Function();

    static Arguments[] getTheDayOfTheWeekTestArgs(){
        return new Arguments[]{
                Arguments.arguments(1, "Monday"),
                Arguments.arguments(2, "Tuesday"),
                Arguments.arguments(3, "Wednesday"),
                Arguments.arguments(4, "Thursday"),
                Arguments.arguments(5, "Friday"),
                Arguments.arguments(6, "Saturday"),
                Arguments.arguments(7, "Sunday"),
                Arguments.arguments(-20, "Error")
        };
    }
    @ParameterizedTest
    @MethodSource("getTheDayOfTheWeekTestArgs")
    void getTheDayOfTheWeekTest (int a, String expected)
    {
        String actual = function.getTheDayOfTheWeek(a);
        Assertions.assertEquals(expected, actual);
    }


    static Arguments[] getDistanceBetweenPointsTestArgs(){
        return new Arguments[]{
                Arguments.arguments(0, 0, 0, 3, 3),
                Arguments.arguments(2, 2, 2, 8, 6)
        };
    }
    @ParameterizedTest
    @MethodSource("getDistanceBetweenPointsTestArgs")
    void getDistanceBetweenPointsTest (double x1, double y1, double x2, double y2,  double expected)
    {
        double actual = function.getDistanceBetweenPoints(x1,y1,x2,y2);
        Assertions.assertEquals(expected, actual);
    }



    static Arguments[] getTheNameOfTheNumberTestArgs(){
        return new Arguments[]{
                Arguments.arguments(2, "два"),
                Arguments.arguments(115, "сто пятнадцать"),
                Arguments.arguments(872, "восемьсот семьдесят два"),
                Arguments.arguments(300, "триста"),
        };
    }
    @ParameterizedTest
    @MethodSource("getTheNameOfTheNumberTestArgs")
    void getTheNameOfTheNumberTest (int a, String expected)
    {
        String actual = function.getTheNameOfTheNumber(a);
        Assertions.assertEquals(expected, actual);
    }


    static Arguments[] getTheNumberOfTheNameTestArgs(){
        return new Arguments[]{
                Arguments.arguments("два",2),
                Arguments.arguments("сто пятнадцать",115),
                Arguments.arguments("восемьсот семьдесят два", 872),
                Arguments.arguments("трита", -1),
        };
    }
    @ParameterizedTest
    @MethodSource("getTheNumberOfTheNameTestArgs")
    void getTheNumberOfTheNamerTest (String a, int expected)
    {
        int actual = function.getTheNumberOfTheName(a);
        Assertions.assertEquals(expected, actual);
    }




}
