package test.java.com.homework_1;

import homework_1.java.com.homework.Arrays;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class ArraysTest {



    Arrays cut = new Arrays();


    static Arguments[] getMinTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new double[]{3, 5, 2, 6, 3, 8, 5, 1, 88, 44, 62, 5}, 1),
                Arguments.arguments(new double[]{2.44, 55,645,543,2,334.55,-1.4443,434,-1.442}, -1.4443)

        };
    }
    @ParameterizedTest
    @MethodSource("getMinTestArgs")
    void getMinTest (double[] arr, double expected)
    {
        double actual = cut.getMin(arr);
        Assertions.assertEquals(expected, actual);
    }




    static Arguments[] getMaxTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new double[]{3, 5, 2, 6, 3, 8, 5, 1, 88, 44, 62, 5}, 88),
                Arguments.arguments(new double[]{2.44, 55,645,543,2,334.55,-1.4443,434,-1.442}, 645)

        };
    }
    @ParameterizedTest
    @MethodSource("getMaxTestArgs")
    void getMaxTest (double[] arr, double expected)
    {
        double actual = cut.getMax(arr);
        Assertions.assertEquals(expected, actual);
    }




    static Arguments[] getIndexMinTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new double[]{3, 5, 2, 6, 3, 8, 5, 1, 88, 44, 62, 5}, 7),
                Arguments.arguments(new double[]{2.44, 55,645,543,2,334.55,-1.4443,434,-1.442}, 6)

        };
    }
    @ParameterizedTest
    @MethodSource("getIndexMinTestArgs")
    void getIndexMinTest (double[] arr, int expected)
    {
        int actual = cut.getIndexMin(arr);
        Assertions.assertEquals(expected, actual);
    }





    static Arguments[] getIndexMaxTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new double[]{3, 5, 2, 6, 3, 8, 5, 1, 88, 44, 62, 5}, 8),
                Arguments.arguments(new double[]{2.44, 55,645,543,2,334.55,-1.4443,434,-1.442}, 2)

        };
    }
    @ParameterizedTest
    @MethodSource("getIndexMaxTestArgs")
    void getIndexMaxTest (double[] arr, int expected)
    {
        int actual = cut.getIndexMax(arr);
        Assertions.assertEquals(expected, actual);
    }




    static Arguments[] getSumOddIndexOfElementsTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new double[]{3, 5, 2, 6, 3, 8, 5, 1, 88, 44, 62, 5}, 69),
                Arguments.arguments(new double[]{2.44, 55, 645, 543, 2, 334.55, -1.4443, 434, -1.442}, 1366.55)

        };
    }
    @ParameterizedTest
    @MethodSource("getSumOddIndexOfElementsTestArgs")
    void getSumOddIndexOfElementsTest (double[] arr, double expected)
    {
        double actual = cut.getSumOddIndexOfElements(arr);
        Assertions.assertEquals(expected, actual);
    }




    static Arguments[] getReverseArrayTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{3, 5, 2, 6}, new int[]{6, 2, 5, 3}),
                Arguments.arguments(new int[]{1, 2, 3, 4}, new int[]{4, 3, 2, 1})

        };
    }
    @ParameterizedTest//ИСПРАВИТЬ!
    @MethodSource("getReverseArrayTestArgs")
    void getReverseArrayTest (int[] arr, int[] expected)
    {
        int[] actual = cut.getReverseArray(arr);
        Assertions.assertArrayEquals(expected, actual);
    }





    static Arguments[] getNumberOfOddElementsTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new double[]{3, 5, 2, 6}, 2),
                Arguments.arguments(new double[]{1, 2, 3, 4}, 2)

        };
    }
    @ParameterizedTest
    @MethodSource("getNumberOfOddElementsTestArgs")
    void getNumberOfOddElementsTest (double[] arr, int expected)
    {
        int actual = cut.getNumberOfOddElements(arr);
        Assertions.assertEquals(expected, actual);
    }



    static Arguments[] getLeftSwitchRightArrayTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new double[]{3, 5, 2, 6}, new double[]{2, 6, 3, 5}),
                Arguments.arguments(new double[]{1, 2, 3, 4}, new double[]{3, 4, 1, 2}),
                Arguments.arguments(new double[]{1, 2, 3, 4, 5}, new double[]{1, 2, 3, 4, 5})
        };
    }
    @ParameterizedTest//Исправить!!!
    @MethodSource("getLeftSwitchRightArrayTestArgs")
    void getLeftSwitchRightArrayTest (double[] arr, double[] expected)
    {
        double[] actual = cut.getLeftSwitchRightArray(arr);
        Assertions.assertArrayEquals(expected, actual);
    }



    static Arguments[] sortBubbleTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new double[]{3, 5, 2, 6}, new double[]{2, 3, 5, 6}),
                Arguments.arguments(new double[]{99, 42, -1, 54, 49}, new double[]{-1, 42, 49, 54,99})

        };
    }
    @ParameterizedTest//Исправить!!!
    @MethodSource("sortBubbleTestArgs")
    void sortBubbleTest (double[] arr, double[] expected)
    {
        double[] actual = cut.sortBubble(arr);
        Assertions.assertArrayEquals(expected, actual);
    }



    static Arguments[] sortSelectTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new double[]{3, 5, 2, 6}, new double[]{2, 3, 5, 6}),
                Arguments.arguments(new double[]{99, 42, -1, 54, 49}, new double[]{-1, 42, 49, 54,99})

        };
    }
    @ParameterizedTest//Исправить!!!
    @MethodSource("sortSelectTestArgs")
    void ssortSelectTest (double[] arr, double[] expected)
    {
        double[] actual = cut.sortSelect(arr);
        Assertions.assertArrayEquals(expected, actual);
    }




    static Arguments[] sortInsertTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new double[]{3, 5, 2, 6}, new double[]{2, 3, 5, 6}),
                Arguments.arguments(new double[]{99, 42, -1, 54, 49}, new double[]{-1, 42, 49, 54,99})

        };
    }
    @ParameterizedTest//Исправить!!!
    @MethodSource("sortInsertTestArgs")
    void sortInsertTest (double[] arr, double[] expected)
    {
        double[] actual = cut.sortInsert(arr);
        Assertions.assertArrayEquals(expected, actual);
    }






}
