package test.java.com.homework_1;

import homework_1.java.com.homework.ConditionalOperators;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class ConditionalOperatorsTest {

    ConditionalOperators cut = new ConditionalOperators();

    static Arguments[] getMultiplicationOrSumTestArgs(){
        return new Arguments[]{
                Arguments.arguments(4, 3, 12),
                Arguments.arguments(7, 23, 30)

        };
    }
    @ParameterizedTest
    @MethodSource("getMultiplicationOrSumTestArgs")
    void getMultiplicationOrSumTest (double a, double b, double expected)
    {
        double actual = cut.getMultiplicationOrSum(a,b);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getQuarterCoordinatesTestArgs(){
        return new Arguments[]{
                Arguments.arguments(4, 3, 1),
                Arguments.arguments(-4, 3, 2),
                Arguments.arguments(-4, -3, 3),
                Arguments.arguments(4, -3, 4),
                Arguments.arguments(0, 0, 0),
                Arguments.arguments(3, 0, 0),
                Arguments.arguments(0, 4, 0),
                Arguments.arguments(-3, 0, 0),
                Arguments.arguments(0, -4, 0)

        };
    }
    @ParameterizedTest
    @MethodSource("getQuarterCoordinatesTestArgs")
    void getQuarterCoordinatesTest (double x, double y, int expected)
    {
        double actual = cut.getQuarterCoordinates(x,y);
        Assertions.assertEquals(expected, actual);
    }


    static Arguments[] GetSumOfPositivesTestArgs(){
        return new Arguments[]{
                Arguments.arguments(4, 3, 1, 8),
                Arguments.arguments(-4, 3, 2, 5),
                Arguments.arguments(4, -3, 2, 6),
                Arguments.arguments(4, 3, -2, 7),
                Arguments.arguments(-4, -3, 2, 2),
                Arguments.arguments(-4, 3, -2, 3),
                Arguments.arguments(4, -3, -2, 4),
                Arguments.arguments(-4, -3, -2, 0),


        };
    }
    @ParameterizedTest
    @MethodSource("GetSumOfPositivesTestArgs")
    void GetSumOfPositivesTest (double a, double b, double c, double expected)
    {
        double actual = cut.GetSumOfPositives(a,b,c);
        Assertions.assertEquals(expected, actual);
    }
    static Arguments[] getMaximumPlusThreeTestArgs(){
        return new Arguments[]{
                Arguments.arguments(4, 3, 1, 15),
                Arguments.arguments(1, 3, 2, 0),
                Arguments.arguments(1, 1, 2, 7)


        };
    }
    @ParameterizedTest
    @MethodSource("getMaximumPlusThreeTestArgs")
    void getMaximumPlusThreeTest (double a, double b, double c, double expected)
    {
        double actual = cut.getMaximumPlusThree(a,b,c);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getСategoryOfMarkTestArgs(){
        return new Arguments[]{
                Arguments.arguments(-20, "Not found"),
                Arguments.arguments(101, "Not found"),
                Arguments.arguments(15, "F"),
                Arguments.arguments(0, "F"),
                Arguments.arguments(19, "F"),
                Arguments.arguments(20, "E"),
                Arguments.arguments(27, "E"),
                Arguments.arguments(39, "E"),
                Arguments.arguments(40, "D"),
                Arguments.arguments(58, "D"),
                Arguments.arguments(59, "D"),
                Arguments.arguments(60, "C"),
                Arguments.arguments(70, "C"),
                Arguments.arguments(74, "C"),
                Arguments.arguments(75, "B"),
                Arguments.arguments(79, "B"),
                Arguments.arguments(89, "B"),
                Arguments.arguments(90, "A"),
                Arguments.arguments(96, "A"),
                Arguments.arguments(100, "A")
        };
    }
    @ParameterizedTest
    @MethodSource("getСategoryOfMarkTestArgs")
    void getСategoryOfMarkTest (double mark, String expected)
    {
        String actual = cut.getСategoryOfMark(mark);
        Assertions.assertEquals(expected, actual);
    }
}
