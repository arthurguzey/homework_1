package homework_1.java.com.homework;

public class Cycles {
    public static int getSumOfEvenFromOneToNinetynine()
    {//задание 1
        int sum=0;
        int kol=0;
        for(int i=1;i<100;i++)
        {
            if(i%2==0)
            {
                sum+=i;
                kol++;
            }
        }
        System.out.println("Сумма четных чисел = "+sum);
        System.out.println("Количество четных чисел = "+kol);
        return  sum;
    }
    public static boolean checkPrimeNumber(int a)
    {//задание 2
        boolean result = true;
        if (a < 0)
        {
            a=a*(-1);
        }
        for (int i = 2; i < a; i++)
        {
            if(a%i == 0)
            {
                result = false;
            }
        }
        if(result == true)
        {
            System.out.println("Число простое");
        }
        else
        {
            System.out.println("Число не простое");
        }
        return result;
    }
    public static int getSqrt(int a)
    {//задание 3
        if(a < 0)
        {
            System.out.println("Not found");
            return  0;
        }
        for (int i=1; ; i++ )
        {
            int q = i * i;
            if (a == q)
            {
                return i;
            }
            if (a < q)
            {
                return i - 1;
            }
        }
    }
    public static int getFactorial(int a)
    {//задание 4
        int result = 1;
        if (a < 0)
        {
            System.out.println("Error");
            result=0;
        }
        for (int i = 1; i <= a; i++)
        {
            result = result * i;
        }
        return result;
    }
    public static int getSumOfDigits(int n)
    {//задание 5
        int sum = 0;
        if (n < 0)
        {
            n=n*(-1);
        }
        while (n != 0)
        {
            sum = sum + (n % 10);
            n /= 10;
        }
        return sum;
    }
    public static int getMirrorNumber(int n)
    {//задание 6 (надо проверить)
        if (n < 0)
        {
            n=n*(-1);
        }
        int mirror = 0;
        int i = 0;
        while ( n > 0 )
        {
            if(i == 0)
            {
                mirror=n%10;
            }
            else
            {
                mirror=mirror*10;
                mirror=mirror+n%10;
            }
            i++;
            //System.out.print(n%10);
            n/=10;
        }
        return  mirror;
    }
}
