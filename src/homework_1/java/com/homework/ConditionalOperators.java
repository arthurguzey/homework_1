package homework_1.java.com.homework;

public class ConditionalOperators {
    public static double getMultiplicationOrSum(double a, double b)
    {//задание 1
        double result = 0;
        if(a%2==0)
        {
            result = a*b;
        }
        else
        {
            result = a+b;
        }

        //double scale = Math.pow(10, 3);     округление
        //result = Math.ceil(result * scale) / scale;
        return result;
    }
    public static int getQuarterCoordinates(double x, double y)
    {//задание 2
        int quarter=0;
        if(x>0 && y>0)
        {
            System.out.println("Точка находяться в 1 четверти");
            quarter= 1;
        }
        if(x<0 && y>0)
        {
            System.out.println("Точка находяться во 2 четверти");
            quarter= 2;
        }
        if(x<0 && y<0)
        {
            System.out.println("Точка находяться в 3 четверти");
            quarter= 3;
        }
        if(x>0 && y<0)
        {
            System.out.println("Точка находяться в 4 четверти");
            quarter= 4;
        }
        if(x==0 && y==0)
        {
            System.out.println("Точка находяться в начале координат");
            quarter= 0;
        }
        if(x==0 && y>0)
        {
            System.out.println("Точка находяться на положительной части оси ОУ");
            quarter= 0;
        }
        if(x==0 && y<0)
        {
            System.out.println("Точка находяться на отрицательной части оси ОУ");
            quarter= 0;
        }
        if(x>0 && y==0)
        {
            System.out.println("Точка находяться на положительной части оси ОX");
            quarter= 0;
        }
        if(x<0 && y==0)
        {
            System.out.println("Точка находяться на отрицательной части оси ОX");
            quarter= 0;
        }
        return quarter;
    }
    public static double GetSumOfPositives(double a, double b, double c)
    {//задание 3
        int sum = 0;
        if( a > 0 )
        {
            sum += a;
        }
        if( b > 0 )
        {
            sum += b;
        }
        if( c > 0 )
        {
            sum += c;
        }
        return  sum;
    }
    public static double getMaximumPlusThree(double a, double b, double c)
    {//задание 4
        double g = a * b * c;
        double h = a + b + c;
        double m = 0;
        if( g > h )
        {
            m = g;
        }
        if( g < h )
        {
            m = h;
        }
        if( g == h )
        {
            System.out.println("Значения равны между собой");
            m=-3;
        }
        m=m+3;
        return  m;
    }
    public static String getСategoryOfMark(double mark)
    {//задание 5
        String categoryofmark=" ";
        if(mark<0 || mark>100)
        {
            categoryofmark = "Not found";
            return categoryofmark;
        }
        if (mark>=0 && mark<=19)
        {
            categoryofmark="F";
        }
        if (mark>=20 && mark<=39)
        {
            categoryofmark="E";
        }
        if (mark>=40 && mark<=59)
        {
            categoryofmark="D";
        }
        if (mark>=60 && mark<=74)
        {
            categoryofmark="C";
        }
        if (mark>=75 && mark<=89)
        {
            categoryofmark="B";
        }
        if (mark>=90 && mark<=100)
        {
            categoryofmark="A";
        }
        return categoryofmark;
    }
}
