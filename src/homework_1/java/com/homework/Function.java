package homework_1.java.com.homework;

public class Function {
    public static String getTheDayOfTheWeek(int numberday)
    {//задание 1
        String name = " ";
        switch (numberday){
            case (1):
                name="Monday";
                break;
            case (2):
                name="Tuesday";
                break;
            case (3):
                name="Wednesday";
                break;
            case (4):
                name="Thursday";
                break;
            case (5):
                name="Friday";
                break;
            case (6):
                name="Saturday";
                break;
            case (7):
                name="Sunday";
                break;
            default:
                name="Error";
                System.out.println("You entered an incorrect value");
                break;
        }
        return name;
    }
    public static double getDistanceBetweenPoints (double x1, double y1, double x2, double y2)
    {//задание 2
        double distancex = Math.abs(x2-x1);
        double distancey = Math.abs(y2-y1);
        double distance = Math.sqrt(Math.abs(Math.pow(distancex,2)-Math.pow(distancey,2)));
        return distance;
    }
    public static String getTheNameOfTheNumber (int number)
    {//задание 3
        String[] belowTwenty = {"ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
        String[] tens = {"сто", "десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
        String[] hundreds = {"тысяча", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};
        String soFar;
        if (number % 100 < 20 && number != 100){
            soFar = belowTwenty[number % 100];
            number /= 100;
        }
        else {
            if (number == 100) {
                return (tens[0]);
            }
            soFar = belowTwenty[number % 10];
            number /= 10;

            soFar = tens[number % 10] + " " + soFar;
            number /= 10;
        }
        if (number == 0) {
            return soFar;
        }
        if(soFar.equals(belowTwenty[0]))
        {
            return hundreds[number];
        }
        return hundreds[number] + " " + soFar;
    }
    public static int getTheNumberOfTheName (String name)
    {//задание 4
        int number = 0;
        boolean correctName = false;
        String[] belowTwenty = {"ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
        int belowTwentyLenght = belowTwenty.length;
        String[] tens = {"десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
        int tensLenght = tens.length;
        String[] hundreds = {"сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};
        int hundredsLenght = hundreds.length;
        String[] wordsOfNumber = name.split(" ");
        int wordsOfNumberLenght = wordsOfNumber.length;
        for (int j=0; j<wordsOfNumberLenght; j++)
        {
            for(int i = 0; i < belowTwentyLenght; i++)
            {
                if (wordsOfNumber[j].equals(belowTwenty[i]))
                {
                    correctName = true;
                    number = number + i;
                }
            }
            for(int i = 0; i < tensLenght; i++)
            {
                if (wordsOfNumber[j].equals(tens[i]))
                {
                    correctName = true;
                    number = number + ((i+1)*10);
                }
            }
            for(int i = 0; i < hundredsLenght; i++)
            {
                if (wordsOfNumber[j].equals(hundreds[i]))
                {
                    correctName = true;
                    number = number + ((i+1)*100);
                }
            }
        }

        if(correctName=false){
            System.out.println("Error");
            number = -1;
        }
        return number;
    }


}
