package homework_1.java.com.homework;

import java.sql.SQLOutput;

public class Arrays {
    public static double getMin(double[] array)
    {//задание 1
        double min = 999999999;
        for (int i = 0; i < array.length; i++)
        {
            if(min>array[i])
            {
                min = array[i];
            }

        }
        return min;
    }

    public static double getMax(double[] array)
    {//задание 2
        double max = -999999999;
        for (int i = 0; i < array.length; i++)
        {
            if(max<array[i])
            {
                max = array[i];
            }

        }
        return max;
    }
    public static int getIndexMin(double[] array)
    {//задание 3
        double min = 999999999;
        int k=-1;
        for (int i = 0; i < array.length; i++) {
            if(min>array[i])
            {
                min = array[i];
                k=i;
            }
        }
        return k;
    }
    public static int getIndexMax(double[] array)
    {//задание 4
        double max = -999999999;
        int k=-1;
        for (int i = 0; i < array.length; i++)
        {
            if(max<array[i])
            {
                max = array[i];
                k=i;
            }
        }
        return k;
    }
    public static double getSumOddIndexOfElements(double[] array)
    {//задание 5
        double sum=0;
        for (int i = 0; i < array.length; i++)
        {
            if(i%2!=0)
            {
                sum+=array[i];
            }
        }
        return sum;
    }
    public static int[] getReverseArray(int[] array)
    {//задание 6
        int[] array2 = new int[array.length];
        int j=0;
        for (int i = (array.length-1); i >=0 ; i--)
        {
            array2[j] = array[i];
            System.out.print(array2[j] +" ");
            j++;
        }
        
        return array2;
    }

    public static int getNumberOfOddElements(double[] array)
    {//задание 7
        int k=0;
        for (int i = 0; i < array.length; i++)
        {
            if(array[i]%2!=0)
            {
                k++;
            }
        }
        return k;
    }

    public static double[] getLeftSwitchRightArray(double[] array)
    {//задание 8
        if(array.length%2!=0)
        {
            System.out.println("Массив должен быть из n*2 елементов!");
            return array;
        }
        for(int i=0; i<array.length/2;i++)
        {
            double t = array[i];
            array[i] = array[array.length/2+i];
            array[array.length/2+i] = t;
        }
        for (int i = 0; i<array.length; i++)
        {
            System.out.print(array[i] + " ");
        }

        return array;
    }
    public static double[] sortBubble(double[] array)
    {//задание 9
        boolean isSorted = false;
        double t;
        while(!isSorted)
        {
            isSorted = true;
            for (int i = 0; i < array.length-1; i++)
            {
                if(array[i] > array[i+1])
                {
                    isSorted = false;
                    t = array[i];
                    array[i] = array[i+1];
                    array[i+1] = t;
                }
            }
        }
        for (int i = 0; i<array.length; i++)
        {
            System.out.print(array[i] + " ");
        }
        return array;

    }
    public static double[] sortSelect(double[] array)
    {
        int arraylength = array.length;
        for (int i = 0; i < arraylength; i++)
        {
            int pos = i;
            double min = array[i];
            for (int j = i + 1; j < arraylength; j++)
            {
                if (array[j] < min)
                {
                    pos = j;
                    min = array[j];
                }
            }
            array[pos] = array[i];
            array[i] = min;
        }
        for (int i = 0; i<array.length; i++)
        {
            System.out.print(array[i] + " ");
        }
        return array;
    }
    public static double[] sortInsert(double[] array)
    {
        int arraylength = array.length;
        for(int i = 1; i <= (arraylength-1); i++)
        {
            double x = array[i];
            int j = i;
            while(j > 0 && array[j-1] > x)
            {
                array[j] = array[j-1];
                j--;
            }
            array[j]=x;
        }
        for (int i = 0; i<array.length; i++)
        {
            System.out.print(array[i] + " ");
        }
        return array;
    }

}
